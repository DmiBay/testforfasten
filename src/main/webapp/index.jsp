<%@page import="java.util.UUID"%>
<%@page import="ru.bay.fasten.model.MessageType"%>
<html>
<head>
<meta charset="UTF-8">
<title>Test WebSocket</title>
<link rel="stylesheet" href="styles.css" />
<script src="sockjs-1.0.3.js"></script>
<script src="stomp.js"></script>
<script type="text/javascript">
        var stompClient = null;
        var ctxPath = "<%=request.getContextPath()%>";
		var sequenceId = "<%=UUID.randomUUID().toString()%>";
		var token = null;
		
	function setConnected(connected) {
		document.getElementById('connect').disabled = connected;
		document.getElementById('disconnect').disabled = !connected;
		document.getElementById('controlsDiv').style.visibility = connected ? 'visible'	: 'hidden';
		document.getElementById('response').innerHTML = '';
	}

	function connect() {
		var socket = new SockJS(ctxPath + '/auth');
		stompClient = Stomp.over(socket);
		stompClient.connect({}, function(frame) {
			setConnected(true);
			console.log('Connected: ' + frame);
			console.log('SequenceId: ' + sequenceId);
			stompClient.subscribe('/user/queue/login',
					function(message) {
						saveToken(message.body);
						showMessage(message.body);
					});
			stompClient.subscribe('/user/queue/history',
					function(message) {
						showMessage(message.body);
					});
		});
	}

	function disconnect() {
		stompClient.disconnect();
		setConnected(false);
		console.log("Disconnected");
	}

	function sendName(period) {
		if (!stompClient.connected)
			return;
		
		var login = document.getElementById('login').value;
		var pass = document.getElementById('pass').value;
		stompClient.send("/app/login", {}, JSON.stringify({
			'type' : '<%=MessageType.LOGIN_CUSTOMER.toString()%>',
			'sequence_id' : sequenceId,
			'data' : {
				'email' : login,
				'password' : pass
			}
		}));
		if (period > 0) {
			setTimeout(function(){sendName(period);}, period);
		}
	}
	
	function getHistory() {
		if (!stompClient.connected)
			return;
		
		stompClient.send("/app/history", {}, JSON.stringify({
			'type' : '<%=MessageType.TOKEN_HISTORY.toString()%>',
			'sequence_id' : sequenceId,
			'data' : token
		}));
	}

	function saveToken(strMessage) {
		var message = JSON.parse(strMessage);
		if ('<%=MessageType.CUSTOMER_API_TOKEN.toString()%>' == message.type) {
			token = message.data.api_token;
		}
	}
	
	function showMessage(strMessage) {
		var message = JSON.parse(strMessage);
		var response = document.getElementById('response');
		var messagesBox = document.getElementById('messages');
		
		var p = document.createElement('p');
		p.style.wordWrap = 'break-word';
		p.textContent = JSON.stringify(message, null, 2);
		if (message.type) {
			p.className = message.type;
		}
		response.appendChild(p);
		messagesBox.scrollTop = messagesBox.scrollHeight;
	}
</script>
</head>
<body>
	<noscript>
		<h2 style="color: #ff0000">Seems your browser doesn't support
			Javascript! Websocket relies on Javascript being enabled. Please
			enable Javascript and reload this page!</h2>
	</noscript>
	<div id="main">
		<div>
			<button id="connect" onclick="connect();">Connect</button>
			<button id="disconnect" disabled="disabled" onclick="disconnect();">Disconnect</button>
		</div>
		<div id="controlsDiv" style="visibility:hidden" >
			<div id="loginDiv">
				<div>
					<label for="login" >Email</label>
					<input type="text" id="login" />
				</div>
				<div>
					<label for="pass">Password</label>
					<input type="password" id="pass" />
				</div>
			</div>
			<div id="buttonsDiv">
				<button id="sendName" onclick="sendName(0);">Send</button>
				<button id="sendPeriodically" onclick="sendName(2000);">Send periodically</button>
				<button id="getHistory" onclick="getHistory();">History</button>
			</div>
		</div>
	</div>
	<div id="messages">
		<pre id="response"></pre>
	</div>
</body>
</html>