package ru.bay.fasten.service;

import java.util.Calendar;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import ru.bay.fasten.AuthException;
import ru.bay.fasten.InvalidTokenException;
import ru.bay.fasten.dao.AccountDAO;
import ru.bay.fasten.dao.TokenDAO;
import ru.bay.fasten.model.entity.Account;
import ru.bay.fasten.model.entity.Token;

/**
 * Authentication Service implementation
 * @author Dmitriy
 */
@Service("authService")
public class AuthServiceImpl implements AuthService {

	//time before token expiration in minutes
	private static final int TOKEN_LIVETIME = 1; 
	
	private final Map<String, Token> tokens = new ConcurrentHashMap<String, Token>();
	
	@Autowired
	private AccountDAO accountDao;
	
	@Autowired
	private TokenDAO tokenDao;
	
	@Override
	@Transactional
	public Token authenticate(String login, String pass) throws AuthException {
		Assert.notNull(login);
		Assert.notNull(pass);		
		Account account = accountDao.findByLoginAndPass(login, pass);
		if (account == null) {			
			throw new AuthException("Customer not found");
		}
		//creation
		Token token = createToken(account);
		
		//check for recently used token
		Token oldToken = tokens.get(account.getLogin());
		if (oldToken != null) {
			oldToken.setActive(false);
			tokenDao.update(oldToken);
		}
		
		tokens.put(account.getLogin(), token);
		return token;	
	}

	private Token createToken(Account account) {
		Token token = new Token();
		token.setAccount(account);
		token.setActive(true);
		token.setUuid(UUID.randomUUID().toString());
		
		Calendar calendar = Calendar.getInstance();
		token.setCreationDate(calendar);
		
		calendar.add(Calendar.MINUTE, TOKEN_LIVETIME);
		token.setExpirationDate(calendar);
		tokenDao.create(token);

		return token;
	}

	@Override
	@Transactional(readOnly=true)
	public List<Token> getTokenHistory(String login) {
		Assert.notNull(login);
		List<Token> tokens = tokenDao.findByAccountId(login);
		return tokens.stream()
				.sorted((tkn1, tkn2) -> tkn1.getCreationDate().compareTo(tkn2.getCreationDate()))
				.collect(Collectors.toList());
	}

	@Override
	@Transactional(readOnly=true)
	public void checkToken(String uuid) throws InvalidTokenException {
		Assert.notNull(uuid);
		Token token = tokenDao.findByPk(uuid);
		if (token == null) {
			throw new InvalidTokenException("There is no such token");
		}
		checkToken(token);
	}

	@Override
	public void checkToken(Token token) throws InvalidTokenException {
		Calendar now = Calendar.getInstance(); 
		if (!token.isActive()) {
			throw new InvalidTokenException("Token is inactive");
		}
		if (now.after(token.getExpirationDate())) {
			throw new InvalidTokenException("Token is expired");
		}
		if (token.getAccount() == null) {
			throw new InvalidTokenException("Token has no account");
		}
	}

	@Override
	@Transactional(readOnly=true)
	public Token getToken(String tokenUuid) {
		Token token = tokenDao.findByPk(tokenUuid);
		return token;
	}
	
}
