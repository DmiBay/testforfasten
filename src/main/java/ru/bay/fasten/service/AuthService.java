package ru.bay.fasten.service;

import java.util.List;

import ru.bay.fasten.AuthException;
import ru.bay.fasten.InvalidTokenException;
import ru.bay.fasten.model.entity.Token;

/**
 * ������ �������������� � ������ � ��������
 * @author Dmitriy
 */
public interface AuthService {

	/**
	 * �������������� ������������ �� ������ � ������
	 * @param login ����� ������� ������
	 * @param password ������ ������� ������
	 * @return �����
	 * @throws AuthException ������ ��������������, 
	 * ��� ���������� ������ � ������ ����� ������������������ � �������
	 */
	public Token authenticate(String login, String password) throws AuthException;
	
	/**
	 * ��������� ������� ������� ������������
	 * @param login ����� ������������
	 * @return ������������� �������������� ������ �������
	 */
	public List<Token> getTokenHistory(String login);
	
	/**
	 * �������� ������ �� ������������ � ����������
	 * @param uuid ��������� �����
	 * @throws InvalidTokenException ������ ������������ ������, 
	 * ��������� ��� ���������� ������, � ������ ��������� ��� ����� ��������,
	 * ���� � ������ ��� ������������
	 */
	public void checkToken(String uuid) throws InvalidTokenException;
	
	/**
	 * �������� ������ �� ����������
	 * @param token ������-�����
	 * @throws InvalidTokenException ������ ������������ ������,
	 * ��������� � ������, ���� ���� �������� ������ �����, ���� ����� ���������
	 */
	public void checkToken(Token token) throws InvalidTokenException;

	/**
	 * ��������� �������-������ �� �� ���������� ������
	 * @param tokenUuid ��������� �����
	 * @return �������-������ ��� null, ���� �� ������
	 */
	public Token getToken(String tokenUuid);
}
