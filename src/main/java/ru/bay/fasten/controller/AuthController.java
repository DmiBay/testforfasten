package ru.bay.fasten.controller;

import static ru.bay.fasten.model.MessageType.*;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.simp.annotation.SendToUser;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import ru.bay.fasten.AuthException;
import ru.bay.fasten.InvalidTokenException;
import ru.bay.fasten.model.Error;
import ru.bay.fasten.model.Message;
import ru.bay.fasten.model.TokenInfo;
import ru.bay.fasten.model.entity.Token;
import ru.bay.fasten.service.AuthService;

/**
 * ����������, �������������� ��� �������, ��������� � ��������������� 
 * � ��������
 * @author Dmitriy
 */
@Controller
public class AuthController {
	
	@Autowired
	private AuthService authService;
	
	/**
	 * ��������� ������� �� ��������������
	 * @param request �������� ��������� � ������� ��� ��������������
	 * @return ��������� � ����������� ��������������
	 */
	@MessageMapping("/login")
	@SendToUser("/queue/login")
    public Message login(Message request) {
		
		if (LOGIN_CUSTOMER.equals(request.getType())) {
			@SuppressWarnings("unchecked")
			Map<String,Object> accountInfo = (Map<String,Object>) request.getData();
			String login = (String) accountInfo.get("email");
			String pass = (String) accountInfo.get("password");
			Message response;
			
			try {
				Token token = authService.authenticate(login, pass);
				TokenInfo tokenInfo = convert(token);
				response = new Message(CUSTOMER_API_TOKEN, request.getSequenceId(), tokenInfo);
			} catch (AuthException e) {
				Error error = new Error(e.getMessage(), AuthException.CODE);
				response = new Message(CUSTOMER_ERROR, request.getSequenceId(), error);
			}
			return response;
		}
		return new Message();
    }
	
	/**
	 * ��������� ������� �� ��������� ������� �������� �������
	 * @param request ��������� � �������, ���������� ������������� ��� ��������������
	 * @return ������ �������, �����-���� �������� ������� ������������
	 */
	@MessageMapping("/history")
	@SendToUser("/queue/history")
	@Transactional(readOnly=true)
	public Message getHistory(Message request) {
		if (TOKEN_HISTORY.equals(request.getType())) {
			Message response;
			String tokenUuid = (String) request.getData();
			try {
				Token token = checkAndGetToken(tokenUuid);
				String login = token.getAccount().getLogin();
				
				List<Token> tokens = authService.getTokenHistory(login);
				List<TokenInfo> history = tokens.stream()
						.map(tkn -> convert(tkn))
						.collect(Collectors.toList());
				
				response = new Message(TOKEN_HISTORY, request.getSequenceId(), history);
			} catch (InvalidTokenException e) {
				Error error = new Error(e.getMessage(), InvalidTokenException.CODE);
				response = new Message(TOKEN_ERROR, request.getSequenceId(), error);
			}
			return response;
		}
		return new Message();
	}

	
	private Token checkAndGetToken(String tokenUuid) throws InvalidTokenException {
		Token token = authService.getToken(tokenUuid);
		if (token == null) {
			throw new InvalidTokenException("Token is undefined");
		}
		authService.checkToken(token);
		return token;
	}

	private TokenInfo convert(Token token) {
		Assert.notNull(token);
		TokenInfo tokenInfo = new TokenInfo();
		tokenInfo.setUuid(token.getUuid());
		
		String expirationDate = formatDate(token.getExpirationDate());
		tokenInfo.setExpirationDate(expirationDate);
		return tokenInfo;
	}

	private String formatDate(Calendar date) {
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
		return formatter.format(date.getTime());
	}
}
