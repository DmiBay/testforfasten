package ru.bay.fasten.model.entity;

import javax.persistence.Entity;
import javax.persistence.Id;

/**
 * ���������������� �������
 * �������� ��������������� ������ (����� � ������)
 * @author Dmitriy
 */
@Entity
public class Account {

	@Id
	private String login;
	
	private String password;

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

}
