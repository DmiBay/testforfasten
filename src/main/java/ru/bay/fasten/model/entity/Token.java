package ru.bay.fasten.model.entity;

import java.util.Calendar;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

/**
 * ����� ��� ������� � API ����������, 
 * ������� ������������ ��� �������� ��������������
 * @author Dmitriy
 */
@Entity
public class Token {

	@Id
	private String uuid;
	private Calendar expirationDate;
	private Calendar creationDate;
	private boolean active;

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name = "accountId")
	private Account account;

	public String getUuid() {
		return uuid;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

	public Calendar getExpirationDate() {
		return expirationDate;
	}

	public void setExpirationDate(Calendar expirationDate) {
		this.expirationDate = expirationDate;
	}

	public Calendar getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Calendar creationDate) {
		this.creationDate = creationDate;
	}

	public Account getAccount() {
		return account;
	}

	public void setAccount(Account account) {
		this.account = account;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

}
