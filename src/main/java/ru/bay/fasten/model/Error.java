package ru.bay.fasten.model;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * ���������� �� ������, ������� ������ ���� ���������� �������
 * @author Dmitriy
 */
public class Error {

	@JsonProperty("error_description")
	private String description;
	
	@JsonProperty("error_code")
	private String code;

	public Error(String description, String code) {
		this.description = description;
		this.code = code;
	}

	public String getDescription() {
		return description;
	}

	public String getCode() {
		return code;
	}

}
