package ru.bay.fasten.model;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * ����� ����� ���������, 
 * �������� ������������ ����� ����� ������ � ������ 
 * @author Dmitriy
 */
public class Message {
	
	private MessageType type;
	
	@JsonProperty("sequence_id")
	private String sequenceId;
	
	private Object data;
	
	public Message(){};
	
	public Message(MessageType type, String sequenceId, Object data) {
		this.type = type;
		this.sequenceId = sequenceId;
		this.data = data;
	}

	public MessageType getType() {
		return type;
	}

	public String getSequenceId() {
		return sequenceId;
	}

	public Object getData() {
		return data;
	}

	public void setType(MessageType type) {
		this.type = type;
	}

	public void setSequenceId(String sequenceId) {
		this.sequenceId = sequenceId;
	}

	public void setData(Object data) {
		this.data = data;
	}
}
