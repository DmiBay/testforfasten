package ru.bay.fasten.model;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * ���������� � ������, ������� ������ ���� ���������� �������
 * @author Dmitriy
 */
public class TokenInfo {

	@JsonProperty("api_token")
	private String uuid;
	
	@JsonProperty("api_token_expiration_date")
	private String expirationDate;

	public TokenInfo() {
	}

	public String getUuid() {
		return uuid;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

	public String getExpirationDate() {
		return expirationDate;
	}

	public void setExpirationDate(String expirationDate) {
		this.expirationDate = expirationDate;
	};

}
