package ru.bay.fasten.model;

/**
 * ���� ���������
 * @author Dmitriy
 */
public enum MessageType {
	LOGIN_CUSTOMER,		//input message:	login request
	TOKEN_HISTORY,		//in/out message:	history request
	CUSTOMER_API_TOKEN, //output message:	successful login
	CUSTOMER_ERROR,		//output message:	failed login
	TOKEN_ERROR			//output message:	invalid token
}
