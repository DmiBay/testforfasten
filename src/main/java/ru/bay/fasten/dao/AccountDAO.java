package ru.bay.fasten.dao;

import ru.bay.fasten.model.entity.Account;

/**
 * DAO ��� ������ � ����������������� ����������
 * @author Dmitriy
 */
public interface AccountDAO {

	/**
	 * ����� ������� ������ (��������) �� ������ � ������
	 * @return ������� ������������, ���� null, ���� �� ������
	 */
	public Account findByLoginAndPass(String login, String pass);	
}
