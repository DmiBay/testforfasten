package ru.bay.fasten.dao;

import java.util.List;

import ru.bay.fasten.model.entity.Token;

/**
 * DAO ��� ������ � ����������������� ��������
 * @author Dmitriy
 */
public interface TokenDAO {
	
	/**
	 * ����� ������� �� ��������
	 * @param id ������������� ��������
	 * @return ������ �������, ��������� � ���� ���������
	 */
	public List<Token> findByAccountId(String id);
	
	/**
	 * ����� ������ �� uuid
	 * @param uuid ������������� ������
	 * @return �����, ���� null, ���� � ����� uuid ��� ������
	 */
	public Token findByPk(String uuid);
	
	/**
	 * ���������� ������ ������
	 * @return uuid ���������� ������
	 */
	public String create(Token token);
	
	/**
	 * ���������� ������
	 */
	public void update(Token token);
	
}
