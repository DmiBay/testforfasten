package ru.bay.fasten.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import ru.bay.fasten.model.entity.Token;

@Repository
public class TokenDAOImpl implements TokenDAO {

	@Autowired
	private SessionFactory sessionFactory;
	
	@Override
	public List<Token> findByAccountId(String id) {
		@SuppressWarnings("unchecked")
		List<Token> tokens = getSession().createCriteria(Token.class)
				.add(Restrictions.eq("account.login", id)).list();
		return tokens;
	}

	@Override
	public Token findByPk(String uuid) {
		Token token = getSession().get(Token.class, uuid);
		return token;
	}

	@Override
	public String create(Token token) {
		String uuid = (String) getSession().save(token);
		return uuid;
	}

	@Override
	public void update(Token token) {
		getSession().update(token);
	}

	private Session getSession() {
		return sessionFactory.getCurrentSession();
	}
}
