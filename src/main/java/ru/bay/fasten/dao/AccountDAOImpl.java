package ru.bay.fasten.dao;

import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import ru.bay.fasten.model.entity.Account;

@Repository
public class AccountDAOImpl implements AccountDAO {

	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public Account findByLoginAndPass(String login, String pass) {
		Account account = (Account) sessionFactory.getCurrentSession().createCriteria(Account.class)
				.add(Restrictions.eq("login", login))
				.add(Restrictions.eq("password", pass))
				.uniqueResult();
		return account;
	}

}
