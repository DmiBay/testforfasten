package ru.bay.fasten;

/**
 * ������ ���������� ������
 * @author Dmitriy
 */
public class InvalidTokenException extends Exception {

	private static final long serialVersionUID = 1092614132730410365L;
	
	public final static String CODE = "token.isInvalid";
	
	public InvalidTokenException(String message) {
		super(message);
	}
}
